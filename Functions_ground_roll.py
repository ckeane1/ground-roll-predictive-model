
# os to find filepath
import os
# system to find system platform for windows/OSx/Linux
import platform
# pandas to output to csv
import pandas as pd
# numpy for number handling
import numpy as np
# sg to use helper functions
import scipy as sp
from scipy.optimize import fsolve

from datetime import datetime
import math

def my_pitch_up_speed_finder(mass , rho , Cl):

    W = mass * 9.81
    S = 18.5
    Vt = 1.3*((2*W/(rho*Cl*S))**(0.5))

    return(Vt)
        

def my_ground_roll_model( Vias , mass , rho , commanded_torque , stator_temp , prop_pitch ):

    # Constants
    W = mass*9.81 # newtons
    S = 18.5 # m^2
    u = 0.17 # ROLLING FRICTION FOUND FROM TRIAL AND ERROR VS VERIFIED VALUES
    g = 9.81 # m/s^2
    h = 0 # | METERS | (assume the airplane is on the ground and in ground effect)

    integrand = []
    differential = []

    my_thrust = []
    my_lift = []
    my_drag =[]
    my_forcesum = []
    my_friction = []
    for index, V in enumerate(Vias):    

        thrust = get_thrust_single(V, rho, commanded_torque, stator_temp, prop_pitch)[0] # Function for thrust, based on xrotor model

        lift_out = get_lift (V, rho) # Function for lift (however this analysis requires the assumption of design CL = 1)
        lift = lift_out[0]
        CL = lift_out[1]

        drag  = get_drag_ge(V, rho, CL, h )[0] # Function for drag including ground effect


        if(lift < W):
            my_thrust.append(thrust)
            my_lift.append(lift)
            my_drag.append(drag)
            my_friction.append(u*(W-lift))
            my_forcesum.append( thrust - drag - u*(W - lift) )
            integrand.append(V/((g/W)*(thrust - drag - u*(W - lift))))
            differential.append(V)

    #print(integrand)
    print('Avg. Thrust' ,np.average(my_thrust),'Avg. Lift', np.average(my_lift),'Avg. Drag', np.average(drag),'Avg. Friction', np.average(my_friction), 'Rho' , rho)
    sg = np.trapz(integrand, x = differential) # | METERS |
    #plt.plot( Vias,my_thrust)
    #plt.plot( Vias,my_lift)
    #plt.plot( Vias,my_drag)
    #plt.plot( Vias,my_forcesum)
    #plt.plot( differential,integrand )
    #plt.show()
    return(sg)
 

def my_ground_roll_model_average_forces( Vias , mass , rho , h, aoa , commanded_torque , stator_temp , prop_pitch ):
    tc = 0
    lift = 0

    if ( index + 1 ) < len( Vias) & lift < W:

        v2 = Vias[index + 1]
        v1 = Vias[index]

        sg = (mass/2)*(( v2**2 - v1**2 )/( thrust - drag - u*(W - lift) ))
        sg_sum = sg_sum + sg
    return(sg_sum)


def get_thrust_single(n, rho, commanded_torque, stator_temp, prop_pitch):

    output = []
    my_rpm = []
    my_thrust = []
    power_available_thrust = []

    error = 1
 
    coeff_torque_19 = [ -0.0292, 0.0146, 0.016]

    coeff_torque_21 = [ -0.0299, 0.0179, 0.018]

    coeff_thrust_19 = [ -0.1247, -0.0987, 0.2176]

    coeff_thrust_21 = [ -0.1322, -0.076, 0.2319]

    corrected_torque = commanded_torque - (2.35 + 0.024*stator_temp + (0.000379*(stator_temp^2))*(commanded_torque/100))



    rpm_guess = 1

    error = 1

    while abs(error) > 0.01:

        ##CHECK IF THIS GIVES THE RIGHT RPS OR THE NEXT ONE
        rps = rpm_guess / 60

        advance_ratio =  (n) / ((rps) * 1.984)

        val_coeff_torque_19 = np.polyval( coeff_torque_19 , advance_ratio )
        val_coeff_torque_21 = np.polyval( coeff_torque_21 , advance_ratio )

        val_coeff_torque_20 = np.interp(prop_pitch, [19, 21] , [val_coeff_torque_19, val_coeff_torque_21])
        
        torque = (val_coeff_torque_20*rho)*(rps**2)*((1.984)**5)

        error = (corrected_torque - torque ) / (torque)

        rpm_guess = rpm_guess + 1

        #print(error)
        #print(torque)
        #print(corrected_torque)
        #print(rpm_guess)


    val_coeff_thrust_19 = np.polyval( coeff_thrust_19 , advance_ratio )
    val_coeff_thrust_21 = np.polyval( coeff_thrust_21 , advance_ratio )

    val_coeff_thrust_20 = np.interp(prop_pitch, [19, 21] , [val_coeff_thrust_19, val_coeff_thrust_21])

    thrust = val_coeff_thrust_20*rho*(rps**2)*(1.984**4)

    my_thrust = (thrust) 

    my_rpm = (rps*6)

    return(my_thrust, my_rpm)


def get_thrust(Vias, rho, commanded_torque, stator_temp, prop_pitch):

    output = []
    my_rpm = []
    my_thrust = []
    power_available_thrust = []

    error = 1
 
    coeff_torque_19 = [ -0.0292, 0.0146, 0.016]

    coeff_torque_21 = [ -0.0299, 0.0179, 0.018]

    coeff_thrust_19 = [ -0.1247, -0.0987, 0.2176]

    coeff_thrust_21 = [ -0.1322, -0.076, 0.2319]

    corrected_torque = commanded_torque - (2.35 + 0.024*stator_temp + (0.000379*(stator_temp^2))*(commanded_torque/100))

    #for index, n in enumerate(Vias):
    for index, n in enumerate(Vias):
        print(n)
        rpm_guess = 1

        error = 1

        while abs(error) > 0.01:

            ##CHECK IF THIS GIVES THE RIGHT RPS OR THE NEXT ONE
            rps = rpm_guess / 60

            advance_ratio =  (n) / ((rps) * 1.984)

            val_coeff_torque_19 = np.polyval( coeff_torque_19 , advance_ratio )
            val_coeff_torque_21 = np.polyval( coeff_torque_21 , advance_ratio )

            val_coeff_torque_20 = np.interp(prop_pitch, [19, 21] , [val_coeff_torque_19, val_coeff_torque_21])
            
            torque = (val_coeff_torque_20*rho)*(rps**2)*((1.984)**5)

            error = (corrected_torque - torque ) / (torque)

            rpm_guess = rpm_guess + 1

            #print(error)
            #print(torque)
            #print(corrected_torque)
            #print(rpm_guess)


        val_coeff_thrust_19 = np.polyval( coeff_thrust_19 , advance_ratio )
        val_coeff_thrust_21 = np.polyval( coeff_thrust_21 , advance_ratio )

        val_coeff_thrust_20 = np.interp(prop_pitch, [19, 21] , [val_coeff_thrust_19, val_coeff_thrust_21])

        thrust = val_coeff_thrust_20*rho*(rps**2)*(1.984**4)

        my_thrust.append(thrust) 

        my_rpm.append(rps*6)


    return(my_thrust)


def get_lift (V, rho):

    S = 18.5 # m^2

    # Assume aoa = 4 
    CL = 1

    lift = CL * 0.5 * rho * (V**2) * S

    return(lift, CL)

def get_drag(V, rho, CL):
    
    S = 18.5 # m^2

    del_CD = 0.0230552

    ## Valid on a range of -16 < CL < 9
    CD_CL_poly = [ 0.0005, 0.0042, 0.0354 + del_CD ]

    CD = (np.polyval( CD_CL_poly , CL )) #cd #cd

    drag = CD * 0.5 * rho * (V**2) * S

    return(drag,CD)

def get_drag_ge(V, rho, CL, h ): 

    S = 18.5 # m^2

    Cd_o_aeroDB = 0.02658

    del_CD = 0.0230552

    Cd_o = Cd_o_aeroDB + del_CD

    b = 15.24 # m

    h_o = 2.10 # m

    h_real = h + h_o

    phi = ( 16 * ((h_real)/b)**2 )/(1 + ( 16 * (h_real/b))**2)

    # ground effect factor
    CD_CL_poly = [ 0.0005, 0.0042, 0.0354 + del_CD ]

    CD = np.polyval( CD_CL_poly , CL ) #cd

    CD = (CD - Cd_o)*phi + Cd_o

    drag = (CD * 0.5 * rho * V**2 * S)

    return( drag , CD )


def chris_trapz(y , x):
    y_df = pd.DataFrame(y)
    x_df = pd.DataFrame(x)

    delta_x = x_df.diff(periods = 1)

    height = pd.DataFrame(moving_average(y , 1))

    trap_areas = (height.mul(delta_x))

    area_sum = trap_areas.sum()

    return(area_sum)

def moving_average(x, w):
    return np.convolve(x, np.ones(w), 'valid') / w
