
# os to find filepath
import os
# system to find system platform for windows/OSx/Linux
import platform
# pandas to output to csv
import pandas as pd
# numpy for number handling
import numpy as np
# sg to use helper functions
import scipy as sp
from scipy.optimize import fsolve

import plotly.express as px

from Functions_ground_roll import my_pitch_up_speed_finder
from Functions_ground_roll import get_thrust
from Functions_ground_roll import get_drag
from Functions_ground_roll import get_lift
from Functions_ground_roll import my_ground_roll_model
from Functions_ground_roll import chris_trapz


#### MASS SETTINGS  ----- ONLY UNCOMMENT ONE ----- ALWAYS DEFINE IN |POUNDS|

## Define a range of masses with np.arange(start,stop,step)

# mass = np.arange(2132 , 2450 , 50 ) # |POUNDS|

## OR define a specific list of a masses

mass = np.array([4500, 5100]) # |POUNDS|

mass = mass * 0.453592 # conversion to |KG| for calculation

#### ALTITUDE AND CORRESPONDING DENSITY SETTINGS ----- ALWAYS DEFINE IN |KG/(M^3)|
## Define a range of altitudes and their corresponding density

my_altitude_dict = { #-1000 : 1.26128658768,
                     0 : 1.225,   
                            
                     1000 : 1.18954585585,
                     #1500 : 1.17212605171,
                     2000 : 1.15491239911,
                     #2500 : 1.13790489803,
                     3000 : 1.12105201059,
                     #3500 : 1.10440527469,
                     4000 : 1.08796469030,
                     #"4500" : 1.07167871958,
                     5000 : 1.05559890037,
                     # "5500" : 1.0397252327,
                     6000 : 1.02400617867,

                     7000 : 0.99313498732,

                     8000 : 0.962985326311,

                     9000 : 0.933505657772,

                     10000 : 0.904799057464,

                    } # kg/m^3

## OR define a specific list of a masses

# rho = np.array([1.225]) # |POUNDS|


#####   Throttle settings
percent_throttle = 1.0 # Full throttle

commanded_torque = 1000 * percent_throttle # |NEWTON METERS| Throttle setting -> torque commanded, 

stator_temp_average = 65 #|CELCIUS| Found empirically

prop_pitch = 20 # degrees 

#### DATA FRAME DEFINITIONS 

## Data frames for temporary storage
my_datalist_mass = []
my_datalist_mass_pounds = []

my_datalist_density = []
my_datalist_density_slug = []

my_datalist_ground_roll = []
my_datalist_ground_roll_feet = []

my_datalist_altitude = []


## Dataframe for final output to CSV
my_complete_df = pd.DataFrame() 
my_data_df = pd.DataFrame(columns = ["mass (kg)","mass (pounds)","ground roll (m)","ground roll (feet)"]) #creates empty dataframe
my_config_df = pd.DataFrame() 


#### BEGIN CODE

## Iterate first over the range of masses...

for index, m in enumerate(mass):

    W = m*9.81

    S = 18.5 # m^2

    print("Iterating over Mass Config ... " , m)

    ## For each mass, iterate over the range of densities
    
    for altitude in my_altitude_dict:

        rho = my_altitude_dict[altitude]

        print("Iterating over altitude ... " , altitude)

        # Find the approximate takeoff speed (1.3*Vstall)
        Vto = 1.3*( (2*W)/(rho*1*S) )**0.5 # | METERS / SECOND |
        # Create an array of speeds from zero up to the takeoff speed
        Vias = np.arange(1 , Vto + 20 , 1) # m/s

        Slo = my_ground_roll_model(Vias , m , rho , commanded_torque , stator_temp_average , prop_pitch)

        print(Slo* 3.28084)

        my_datalist_altitude.append(altitude)

        my_datalist_mass.append(m)

        my_datalist_mass_pounds.append(str(round(m*2.20462)) + ' pounds') 

        my_datalist_density.append(rho)

        my_datalist_density_slug.append(rho * 0.00194032 )

        my_datalist_ground_roll.append(Slo) 

        my_datalist_ground_roll_feet.append( Slo * 3.28084) 

#print(my_final_datalist_mass)
        
my_data_df["mass (kg)"] = my_datalist_mass
my_data_df["mass (pounds)"] = my_datalist_mass_pounds
my_data_df['Density Altitude (feet)'] = my_datalist_altitude
my_data_df["density (kg/m^3)"] = my_datalist_density
my_data_df["density (slug/ft^3)"] = my_datalist_density_slug
my_data_df["ground roll (m)"] = my_datalist_ground_roll
my_data_df["ground roll (feet)"] = my_datalist_ground_roll_feet

fig = px.line(my_data_df, x='Density Altitude (feet)' , y='ground roll (feet)', color = 'mass (pounds)',title="Takeoff Roll Distance")
fig.show()

nameout= "Groundroll_Distance_Data.csv"

my_data_df.to_csv(nameout,index = False)
print(" DONE!")

